import os
import sys
import glob
from PIL import Image






def main():
	dossier_source  = sys.argv[1] #dossier annotations
	fichier_cible = sys.argv[2] #fichier_csv

	if  os.path.exists(fichier_cible):
		os.system("rm "+ fichier_cible)

	fichier_ecriture=open(fichier_cible, "a")
	fichier_ecriture.write("filename,width,height,class,xmin,ymin,xmax,ymax"+"\n")

	fichiers = [s for s in os.listdir(dossier_source) if s.endswith (".txt")]

	train_counter = 0

	for f in fichiers:
		print (f)
		lecture = open(dossier_source+"/"+ f, "r")
		lignes= lecture.readlines()

		for l in lignes:
			print (l)
			nom = f
			nom_brut = f.split(".")[0]
			img = Image.open(dossier_source+"/"+nom_brut +".jpeg")
			width, height = img.size
			classe = l.split(" ")[4]
			classe=classe.replace("\n","",5)
			#print(classe)
			depart_x=l.split(" ")[0]#str(float(l.split(" ")[0])*float(width))
			depart_y=l.split(" ")[1]
			fin_x=l.split(" ")[2]
			fin_y=l.split(" ")[3]

			if  classe == "Dugong_certain": #only write Dugong_certain annotations to csv file
				train_counter += 1 
				fichier_ecriture.write(dossier_source+"/"+nom_brut+".jpeg" +","+ str(width)+","+  str(height)+","+ classe+","+  str(depart_x)+","+  str(depart_y)  +","+str(fin_x) +","+ str(fin_y)   +"\n")

	print('Number of Dugong_certain annotations :' + str(train_counter))

#filename,width,height,class,xmin,ymin,xmax,ymax
#53.jpeg,1920,1080,fish,808,106,888,254

#1385 227 1420 244 Siganus_rivulatus 

main()
