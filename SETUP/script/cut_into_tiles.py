
import cv2
import os
import numpy as np
import sys


def main():
    
    input_dir = sys.argv[1]
    output_dir = sys.argv[2] #eg '/home/laura/Desktop/Laura_deep/image_data/train/ulm_base3_tiled'
    species = sys.argv[3] #eg 'Dugong_certain'

    #create output dir
    #if os.path.exists(output_dir):
       # raise OSError('output dir already exists')
    #if not os.path.exists(output_dir):
           # os.makedirs(output_dir)

    # list all ulm images
    images = [s for s in os.listdir(input_dir) if s.endswith (".jpeg")]
    #print(images)

    for i in images:

        # read images
        img = cv2.imdecode(np.fromfile(input_dir +'/' + i, dtype=np.uint8), cv2.IMREAD_UNCHANGED)
        #img = cv2.imread(input_dir +'/' + i)
        # get image id
        id = i.split(".")[0]
        print('--------------------')
        print('image_id', id)
        
        # change the current directory to output directory  
        os.chdir(output_dir) 
        
        # cut images into 4 equal tiles and save them in output directory
        for h in range(0,img.shape[0],int(img.shape[0]/2)):
            for w in range(0,img.shape[1],int(img.shape[1]/2)):
                    cv2.imwrite(f"{id}_{w}_{h}.jpeg",img[h:h+int(img.shape[0]/2), w:w+int(img.shape[1]/2),:])
            # name of new image corresponds to coordinates of top left corner
                # image 0_0 has range 0:1352 on x axis and range 0:760 on y axis --> tile 1
                # image 1352_0 has range 1352:2704 on x axis and range 0:760 on y axis -> tile 2
                # image 0_760 has range 0:1352 on x axis and range 760:1520 on y axis -> tile 3
                # image 1352_760 has range 1352:2704 on x axis and range 760:1520 on y axis -> tile 4
        
        #get and read annotation file associated to image
        lecture = open(input_dir + '/' + id + '.txt', "r")
        lignes= lecture.readlines()
        
        #define new bbox coordinates
        new_x1 = 0
        new_y1 = 0
        new_x2 = 0
        new_y2 = 0
        
        for l in lignes:
                #print (l)
                classe = l.split(" ")[4]
                classe=classe.replace("\n","",5)
                    
        # get bounding box coordinates
                if  classe == species:
                    x1= int(l.split(" ")[0])#str(float(l.split(" ")[0])*float(width))
                    y1= int(l.split(" ")[1])
                    x2= int(l.split(" ")[2])
                    y2= int(l.split(" ")[3])

            # check in which tile the bbox falls and abjust bbox coordinates 

                    if x1 in range(0, 1352) and x2 in range(0, 1352) and y1 in range(0, 760) and y2 in range(0, 760):
                        print('bbox belongs to tile 1')
                #substract top left coordinate to get new bbox coordinate
                        new_x1= x1 - 0
                        new_x2= x2 - 0
                        new_y1= y1 - 0
                        new_y2= y2 - 0
                #write in output directory
                        with open(output_dir +'/'+ id + '_0_0.txt','a') as file:
                            file.write(str(new_x1) + ' ' + str(new_y1) + ' ' + str(new_x2) + ' ' + str(new_y2) + ' ' + species + '\n')
                        
                    if x1 in range(1352, 2704) and x2 in range(1352, 2704) and y1 in range(0, 760) and y2 in range(0, 760):
                        print('bbox belongs to tile 2')
                        #substract top left coordinate to get new bbox coordinate
                        new_x1= x1 - 1352
                        new_x2= x2 - 1352
                        new_y1= y1 - 0
                        new_y2= y2 - 0
                #write in output directory
                        with open(output_dir +'/'+ id + '_1352_0.txt','a') as file:
                            file.write(str(new_x1) + ' ' + str(new_y1) + ' ' + str(new_x2) + ' ' + str(new_y2) + ' ' + species + '\n')
            
                    if x1 in range(0, 1352) and x2 in range(0, 1352) and y1 in range(760, 1520) and y2 in range(760, 1520):
                        print('bbox belongs to tile 3')
                        #substract top left coordinate to get new bbox coordinate
                        new_x1= x1 - 0
                        new_x2= x2 - 0
                        new_y1= y1 - 760
                        new_y2= y2 - 760
                #write in output directory
                        with open(output_dir +'/'+ id + '_0_760.txt','a') as file:
                            file.write(str(new_x1) + ' ' + str(new_y1) + ' ' + str(new_x2) + ' ' + str(new_y2) + ' ' + species + '\n')
                
                    if x1 in range(1352, 2704) and x2 in range(1352, 2704) and y1 in range(760, 1520) and y2 in range(760, 1520):
                        print('bbox belongs to tile 4')
                        #substract top left coordinate to get new bbox coordinate
                        new_x1= x1 - 1352
                        new_x2= x2 - 1352
                        new_y1= y1 - 760
                        new_y2= y2 - 760
                #write in output directory
                        with open(output_dir +'/'+ id + '_1352_760.txt','a') as file:
                            file.write(str(new_x1) + ' ' + str(new_y1) + ' ' + str(new_x2) + ' ' + str(new_y2) + ' ' + species + '\n')
		
        
main()
