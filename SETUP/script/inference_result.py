import os
import numpy as np
import sys

true_dir_labels = sys.argv[1]
predict_dir_labels = sys.argv[2]

files = [s for s in os.listdir(predict_dir_labels) if (
    s.endswith(".txt")) and (s.startswith("GH"))]

number_dugong=0
TP=0
FP=0
FN=0

for i in files:
    f_predict = open(predict_dir_labels+ i, 'r')
    num_lines_predict = sum(1 for line in open(
        predict_dir_labels + i, 'r'))
    f_true = open(true_dir_labels + i, 'r')
    
    num_lines_true = sum(1 for line in open(true_dir_labels + i, 'r'))
    

    if num_lines_predict == num_lines_true:
        TP+=1
    elif num_lines_predict <= num_lines_true:
        FN += num_lines_true-num_lines_predict
    else :
        FP += num_lines_predict-num_lines_true
        TP += 1
    number_dugong +=num_lines_true

print('True positive ='+str(TP)+ ' False positive =' + str(FP) +' False negative ='+ str(FN))

print('Recall ='+str(TP/(TP+FN)))
print('Precision ='+str(TP/(TP+FP)))

print('Number of dugong ='+str(number_dugong))

