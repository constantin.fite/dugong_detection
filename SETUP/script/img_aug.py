
# resize images to some percent of their original size and update csv annotation file

#code adapted from https://medium.com/@a.karazhay/guide-augment-images-and-multiple-bounding-boxes-for-deep-learning-in-4-steps-with-the-notebook-9b263e414dac

import sys
print(sys.prefix)
import imgaug as ia
ia.seed(1) #set seed to get same random selection everytime

from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
from imgaug import augmenters as iaa 

# imageio library will be used for image input/output
import imageio
import pandas as pd
import numpy as np
import re
import os
import sys
import glob
import shutil
from shutil import rmtree
import csv


    
# function to convert BoundingBoxesOnImage object into DataFrame

def bbs_obj_to_df(bbs_object):
#     convert BoundingBoxesOnImage object into array
    bbs_array = bbs_object.to_xyxy_array()
#     convert array into a DataFrame ['xmin', 'ymin', 'xmax', 'ymax'] columns
    df_bbs = pd.DataFrame(bbs_array, columns=['xmin', 'ymin', 'xmax', 'ymax'])
    return df_bbs


# create augmentor 
# This setup of augmentation parameters will pick two of seven given augmenters and apply them in random order:

augmentor = iaa.SomeOf(2, [    
    iaa.Affine(scale=(1.1, 1.3)), #Scale images to a value of 110 to 130% of their original size
    iaa.Affine(rotate=(-10, 10)), #Rotate images by -60 to 60 degrees
    iaa.Affine(translate_percent={"x":(-0.1, 0.1),"y":(-0.1, 0.1)}), # Translate images by -10 to +10% on x- and y-axis independently
    iaa.Fliplr(1), #Flip horizontally
    iaa.Flipud(1), #Flip vertically
    iaa.Multiply((0.6, 1.4)), #Multiply all pixels in an image with a specific value, thereby making the image darker or brighter
])


#Next, define imga_ug() function. It will take pandas DataFrame with [‘filename’, ’width’, ’height’, ’class’, ‘xmin’, ‘ymin’, ‘xmax’, ‘ymax’] columns. 
#And apply a number of augmentations to the image and all corresponding bounding boxes.
#The function will return DataFrame with updated images and bounding boxes annotations.

def main():
    
    annotations_file = sys.argv[1] #original csv file of annotations with [‘filename’, ’width’, ’height’, ’class’, ‘xmin’, ‘ymin’, ‘xmax’, ‘ymax’] columns
    image_prefix = sys.argv[2] #suffix for augmented image filenames, e.g ‘aug_’
    iterations = int(sys.argv[3]) #number of times augmentation is applied per image
    #output_dir_labels = sys.argv[4]
    output_dir_img = sys.argv[4]
    output_dir_faster_rcnn = sys.argv[5]
        
    if not os.path.exists(annotations_file):
        raise OSError('path to annotations_file does not exist')

    #make folder to store augmented images
    aug_images_path = os.path.join(os.path.split(annotations_file)[0], "labels_aug/")
    
    if 	os.path.exists(aug_images_path):
        rmtree(aug_images_path)
        print("delete folder")                  
        os.makedirs(aug_images_path)
        
    if not os.path.exists(aug_images_path):
    	os.makedirs(aug_images_path)
    
    	
    if 	os.path.exists(output_dir_img):
        rmtree(output_dir_img)
        print("delete folder")                  
        os.makedirs(output_dir_img)
        
    if not os.path.exists(output_dir_img):
        os.makedirs(output_dir_img)
    
    if  os.path.exists(output_dir_faster_rcnn):  
        rmtree(output_dir_faster_rcnn)
        print("delete folder")
        os.makedirs(output_dir_faster_rcnn)
    if not os.path.exists(output_dir_faster_rcnn):
        os.makedirs(output_dir_faster_rcnn)
    
    # read annotation file into panda dataframe
    df = pd.read_csv(annotations_file)

    grouped = df.groupby('filename')
    
    for filename in df['filename'].unique():
        print(filename)
    #   get separate data frame grouped by file name
        group_df = grouped.get_group(filename)
        group_df = group_df.reset_index()
        group_df = group_df.drop(['index'], axis=1)   
    #   read the image
        image = imageio.imread(filename)
    #   get bounding boxes coordinates and write into array        
        bb_array = group_df.drop(['filename', 'width', 'height', 'class'], axis=1).values
    #   pass the array of bounding boxes coordinates to the imgaug library
        bbs = BoundingBoxesOnImage.from_xyxy_array(bb_array, shape=image.shape)
        
        for i in range(0,iterations):
        	print(i)     
    	#   apply augmentation on image and on the bounding boxes
        	image_aug, bbs_aug = augmentor(image=image, bounding_boxes=bbs)
        	#print(image_aug)
        	#print(bbs_aug)
    	#   disregard bounding boxes which have fallen out of image pane fully or partly   
        	bbs_aug = bbs_aug.remove_out_of_image(fully=True, partly=True)
        
    	#   don't perform any actions with the image if there are no bounding boxes left in it    
        	if re.findall('Image...', str(bbs_aug)) == ['Image([]']:
            		pass
    	#   otherwise continue
        	else:
                    
        		#   write augmented image to a file indexed by i
                    imageio.imwrite(output_dir_img+os.path.splitext(os.path.basename(filename))[0]+image_prefix+str(i)+os.path.splitext(os.path.basename(filename))[1], image_aug)
                    imageio.imwrite(output_dir_faster_rcnn+os.path.splitext(os.path.basename(filename))[0]+image_prefix+str(i)+os.path.splitext(os.path.basename(filename))[1], image_aug)

            		
        		#   create a data frame with augmented values of image width and height
                    info_df = group_df.drop(['xmin', 'ymin', 'xmax', 'ymax'], axis=1)    
                    for index, _ in info_df.iterrows():
                        info_df.at[index, 'width'] = image_aug.shape[1]
                        info_df.at[index, 'height'] = image_aug.shape[0]
                    #   rename filenames by adding the predifined prefix
                    info_df['filename'] = info_df['filename'].apply(lambda x: image_prefix+x)
                    #   create a data frame with augmented bounding boxes coordinates using the function we created earlier
                    bbs_df = bbs_obj_to_df(bbs_aug)
                    #   join class to bounding boxes coordinates 
                    bbs_df2 = bbs_df.join(info_df['class'])
                    #   print(bbs_df2)
                    #   save dataframe into a txt file indexed by i
                    fmt = '%1.4f', '%1.4f', '%1.4f', '%1.4f', '%s'
                    np.savetxt(aug_images_path+os.path.splitext(os.path.basename(filename))[0]+'_aug'+ str(i)+ '.txt', bbs_df2, fmt = fmt)
                    np.savetxt(output_dir_faster_rcnn+os.path.splitext(os.path.basename(filename))[0]+'_aug'+ str(i)+ '.txt', bbs_df2, fmt = fmt)


main()
