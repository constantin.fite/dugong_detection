

import os
import sys
import random
from shutil import copyfile, rmtree

def main():
	
	#splits images into given train, val and test sets based on videos ID

	train_prop = sys.argv[1] # proportion of images for training set (decimal number) - eg 0.60 
	val_prop = sys.argv[2] # proportion of images for validation set (decimal number) - eg 0.20 
	test_prop = sys.argv[3] # proportion of images for test set (decimal number) - eg 0.20 
	species_folder= sys.argv[4] # path to folder of selected species images - eg '/home/laura/Desktop/Laura_deep/image_annots/ulm_base_3_Dugong_certain'
	output_folder_train = sys.argv[5] #path to output folder train - eg '/home/laura/Desktop/Laura_deep/image_data/train/ulm_base3'
	output_folder_val = sys.argv[6] #path to output folder val - eg '/home/laura/Desktop/Laura_deep/image_data/val/ulm_base3'
	output_folder_test = sys.argv[7] #path to output folder test - eg '/home/laura/Desktop/Laura_deep/image_data/test/ulm_base3'



	#### STEP 1: assign species images to training, val and test lists randomly based on video id

	#list all images in species folder
	list_images = [s for s in os.listdir(species_folder) if s.endswith (".jpeg")]

	#get total number of images
	tot_nb = len(list_images) 

	#retrieve all video_ids from image names
	list_id= []
	for image in list_images:
		id = image[:8]
		
		#extract first 8 characters corresponding to video id GHXXXXXX
		list_id.append(id)
		
	  
	#convert to list of unique video ids and randomize
	set_id = set(list_id)
	list_id_unique = list(set_id)
	random.shuffle(list_id_unique)
	
	     
	#for each video, assign all images to train, val and test set until the fixed sampled size of the set is reached
	list_im_train= []
	list_im_val= []
	list_im_test= []  

	train_nb= round(float(train_prop)*tot_nb)
	val_nb= round(float(val_prop)*tot_nb)
	test_nb= round(float(test_prop)*tot_nb)
	
	
	for video in list_id_unique:
		i = 0
	    
		
	    
		if len(list_im_train) <= train_nb:
			 #assign images to train
			list_im_train = list_im_train + [i for i in os.listdir(species_folder) if video in i and i.endswith (".jpeg")]
			print(len(list_im_train))
			print('added to train')
		
		else:
			if len(list_im_val) <= val_nb: #assign images to val
				list_im_val = list_im_val + [i for i in os.listdir(species_folder) if video in i and i.endswith (".jpeg")]
				print('added to val')
		
			else:         
				if len(list_im_test) <= test_nb: #assign remaining images to test
					list_im_test = list_im_test + [i for i in os.listdir(species_folder) if video in i and i.endswith (".jpeg")]
					print('added to test')
		

	print('train images: ', len(list_im_train), ' - val images: ', len(list_im_val), ' - test images: ', len(list_im_test))






	#### STEP 2: copy images (jpeg + txt files) to output folders

	########################### for training set ###########################

	#make folder to store dataset
	if os.path.exists(output_folder_train):
	    rmtree(output_folder_train)
	    print("delete folder")
	    os.makedirs(output_folder_train)
	    #raise OSError('path to output_folder_train already exists')
	if not os.path.exists(output_folder_train):
	    os.makedirs(output_folder_train)
	    print('output_folder_train created')

	counter = 0 
	for image in list_im_train:
	    image_id = image.split('.')
	    txt = image_id[0] + '.txt'
	    copyfile(os.path.join(species_folder, txt), os.path.join(output_folder_train, txt)) #copy txt file to output dir
	    copyfile(os.path.join(species_folder, image), os.path.join(output_folder_train, image)) #copy jpeg file to output dir
	    counter += 1 

	print(counter, ' images copied to ', output_folder_train)
	       
	 
	########################### for val set ###########################

	#make folder to store dataset
	if os.path.exists(output_folder_val):
	    rmtree(output_folder_val)
	    print("delete folder")
	    os.makedirs(output_folder_val)
	    #raise OSError('path to output_folder_val already exists')
	if not os.path.exists(output_folder_val):
	    os.makedirs(output_folder_val)
	    print('output_folder_val created')

	counter = 0 
	for image in list_im_val:
	    image_id = image.split('.')
	    txt = image_id[0] + '.txt'
	    copyfile(os.path.join(species_folder, txt), os.path.join(output_folder_val, txt)) #copy txt file to output dir
	    copyfile(os.path.join(species_folder, image), os.path.join(output_folder_val, image)) #copy jpeg file to output dir
	    counter += 1 

	print(counter, ' images copied to ', output_folder_val)

       
	########################### for test set ###########################

	#make folder to store dataset
	if os.path.exists(output_folder_test):
	    rmtree(output_folder_test)
	    print("delete folder")
	    os.makedirs(output_folder_test)
	    #raise OSError('path to output_folder_test already exists')
	if not os.path.exists(output_folder_test):
	    os.makedirs(output_folder_test)
	    print('output_folder_test created')

	counter = 0 
	for image in list_im_test:
	    image_id = image.split('.')
	    txt = image_id[0] + '.txt'
	    copyfile(os.path.join(species_folder, txt), os.path.join(output_folder_test, txt)) #copy txt file to output dir
	    copyfile(os.path.join(species_folder, image), os.path.join(output_folder_test, image)) #copy jpeg file to output dir
	    counter += 1 

	print(counter, ' images copied to ', output_folder_test)
	      
			        
		
		
main()
        
        
