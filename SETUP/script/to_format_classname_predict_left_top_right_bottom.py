import os
import numpy as np
import sys
from shutil import rmtree

threshold = sys.argv[1]
# r"D:\COURS\EPF\\5ème_Année\LIRMM\été2020\tiles\presence"
input_dir = sys.argv[2]
# r"D:\COURS\EPF\\5ème_Année\LIRMM\yolo_v5\image_annot\ulm_base_3_Dugong_certain"
output_dir = sys.argv[3]

if 	os.path.exists(output_dir):
        rmtree(output_dir)
        print("delete folder")                  
        os.makedirs(output_dir)
        
if not os.path.exists(output_dir):
    	os.makedirs(output_dir)

files = [s for s in os.listdir(input_dir) if (
    s.endswith(".txt")) and (s.startswith("GH"))]
c=0

for i in files:
    f = open(input_dir+"/" + i, 'r')
    with open(output_dir+"/"+i, "w") as f1:
        i = 0
        for line in f:
            if i >= 1:
                f1.write("\n")
            c+=1
            line_split = line.split()
            width = 1352
            height = 760
            x_center = float(line_split[1])
            #print(x_center)
            y_center = float(line_split[2])
            w_o = float(line_split[3])
            h_o = float(line_split[4])
            width_object = w_o*width
            height_object = h_o*height
            x1 = x_center*width-(width_object/2)
            y1 = y_center*height-(height_object/2)

            f1.write("0 "+threshold  + " "+ str(x1) + " " + str(y1)+" " +
                     str(x1+width_object) + " " + str(y1+height_object))
            i += 1
            
print("Nombre d'objet détecté = "+str(c))
