import os
import numpy as np
import sys
from shutil import rmtree

# r"D:\COURS\EPF\\5ème_Année\LIRMM\été2020\tiles\presence"
input_dir = sys.argv[1]
# r"D:\COURS\EPF\\5ème_Année\LIRMM\yolo_v5\image_annot\ulm_base_3_Dugong_certain"
output_dir = sys.argv[2]

if 	os.path.exists(output_dir):
        rmtree(output_dir)
        print("delete folder")                  
        os.makedirs(output_dir)
        
if not os.path.exists(output_dir):
    	os.makedirs(output_dir)
c=0
files = [s for s in os.listdir(input_dir) if (
    s.endswith(".txt")) and (s.startswith("GH"))]

for i in files:
    f = open(input_dir+"/" + i, 'r')
    with open(output_dir+"/"+i, "w") as f1:
        i = 0
        
        for line in f:
            c+=1
            if i >= 1:
                f1.write("\n")
            line_split = line.split()
            width = 1352
            height = 760
            x1 = float(line_split[0])
            y1 = float(line_split[1])
            x2 = float(line_split[2])
            y2 = float(line_split[3])

            f1.write("0 "+str(x1) + " " + str(y1)+" " +
                        str(x2) + " " + str(y2) )
            i += 1

print("Nombre de true Dugong = "+str(c))