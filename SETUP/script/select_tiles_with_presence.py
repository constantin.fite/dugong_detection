

import os
import sys
from shutil import copy

def main():
	
	input_dir = sys.argv[1] #eg '/home/laura/Desktop/Laura_deep/image_data/test/ulm_base3_tiled'
	
	#create output dir
	output_dir = sys.argv[2]
	#output_dir = os.path.join(input_dir + "\\" + "presence")
	#if os.path.exists(output_dir):
	  #  raise OSError('output dir already exists')
	#if not os.path.exists(output_dir):
     #      os.makedirs(output_dir)

	# list all txt files
	txt_files = [s for s in os.listdir(input_dir) if s.endswith (".txt")]

	for txt in txt_files:
		print(txt)
		nom = txt.split(".")[0]
		copy(os.path.join(input_dir, txt), os.path.join(output_dir, txt)) #copy txt file to output dir
		copy(os.path.join(input_dir, txt.split(".")[0] + '.jpeg'), os.path.join(output_dir, txt.split(".")[0] + '.jpeg')) #copy jpeg file to output dir	
        
main()
